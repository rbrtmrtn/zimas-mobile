/*
File:               zmo.map.js
Author:             Robert Martin
Description:        Defines the ZMO.MAP module and handles map content/events.
Last updated:       2013-10-25
 */


// Load Dojo modules
require(
	[
		'esri/map',
		'esri/graphic',
		'esri/InfoTemplate',
		'esri/SpatialReference',
		'esri/geometry/Point',
		'esri/layers/ArcGISTiledMapServiceLayer',
		'esri/layers/ArcGISDynamicMapServiceLayer',
		'esri/layers/FeatureLayer',
		'esri/layers/GraphicsLayer',
		'esri/symbols/PictureMarkerSymbol',
		'esri/symbols/SimpleFillSymbol',
		'esri/symbols/SimpleLineSymbol',
		'esri/tasks/GeometryService',
		'esri/tasks/ProjectParameters',
		'esri/tasks/QueryTask',
		'esri/tasks/query',
		'dojo/_base/Color',
		'dojo/domReady!'
	],
	function (
		Map,
		Graphic,
		InfoTemplate,
		SpatialReference,
		Point,
		ArcGISTiledMapServiceLayer,
		ArcGISDynamicMapServiceLayer,
		FeatureLayer,
		GraphicsLayer,
		PictureMarkerSymbol,
	    SimpleFillSymbol,
	    SimpleLineSymbol,
	    GeometryService,
	    ProjectParameters,
	    QueryTask,
	    Query,
	    Color
	)
	{
		// Private variables
		var map,
			cur = {},
			config,
			layers,

			// Parcel query task
			parcelQueryTask,
			parcelQuery,
            parcelSymbol,
            parcelInfoTemplate,

			// Parcel feature layer
			parcelFeatureLayer,

			// GPS
			// TODO: clean up var names, maybe they should all start with GPS?
			geometryService,
			gpsIsOn = false,
			gpsWatchID,
			gpsGraphicsLayer,
			gpsSymbol,
			gpsGraphic;


		/////////////////////////
		//       ZMO.MAP       //
		/////////////////////////

		ZMO.MAP =
		{
			init: function ()
			{
				// Private references
				config = ZMO.config;
				layers = ZMO.LAYERS.layers();

				// Create map
				var center = new Point(6474270.543663854, 1846784.4815970433, new SpatialReference(2229));
				map = new Map('map', {
					center: center,
					logo: false,
					slider: false,
					zoom: 3
				});

				// Debug stuff
				if(config.debug) {
					gmap = map;  // map global
				}

				// Tell layers to send... layers
				ZMO.LAYERS.mapLoaded();

				// PARCEL QUERY TASK
				var parcelQueryTaskURL = this.urlForMapService('D_QUERYLAYERS') + '/5/query';
				parcelQueryTask = new QueryTask(parcelQueryTaskURL);
				parcelQuery = new Query;
				parcelQuery.returnGeometry = true;
				parcelQuery.outFields = ['PIND'];

				parcelInfoTemplate = new InfoTemplate({
					title: '${primaryAddress}',
					content: '\
						<div><b>PIN</b>: ${PIN}</div>\
						<div><b>Zone</b>: ${zone}</div>\
						<div><b>Land Use</b>: ${landUse}</div>\
						<div style="margin-top: 10px"><a class="parcel-data-link" href="#">More Info</a>\
					'
				});

				parcelSymbol = new SimpleFillSymbol(
					SimpleFillSymbol.STYLE_SOLID,
					new SimpleLineSymbol(
						SimpleLineSymbol.STYLE_SOLID,
						new Color([0.0, 0.0, 255.0]),
						4.0
					),
					new Color([255.0, 0.0, 0.0, 0.0])
				);



				// PARCEL FEATURE LAYER

				map.on('layers-add-result', initSelectToolbar);

				var parcelFeatureLayerURL = this.urlForMapService('D_QUERYLAYERS') + '/5';
				parcelFeatureLayer = new FeatureLayer(parcelFeatureLayerURL, {
					mode: FeatureLayer.MODE_SELECTION,
					outFields: ['PIN', 'PIND']
				});
				parcelFeatureLayer.setSelectionSymbol(parcelSymbol);
				map.addLayers([parcelFeatureLayer]);

				function initSelectToolbar(e) {
					var fl = e.layers[0].layer,
						selectQuery = new Query();

					map.on('click', function(e) {
						selectQuery.geometry = e.mapPoint;
						fl.selectFeatures(selectQuery, FeatureLayer.SELECTION_NEW, function(features) {
							if (features.length > 0)
							{
								var pin = features[0].attributes.PIN,
									calloutData = ZMO.DATA.calloutDataForParcel(pin);

								map.infoWindow.setTitle(calloutData.address);
								map.infoWindow.setContent('\
									<div><b>PIN</b>: ' + pin + '</div>\
									<div><b>Zone</b>: ' + calloutData.zoneCode + '</div>\
									<div><b>Land Use</b>: ' + calloutData.GPLU + '</div>\
									<div style="margin-top: 10px"><a class="parcel-data-link" href="#">More Info</a>\
								');
								map.infoWindow.show(e.screenPoint, map.getInfoWindowAnchor(e.screenPoint));
							}
						});
					});
				}






				// GPS
				var gsURL = 'http://68wzimas1.ci.la.ca.us/ArcGIS/rest/services/Geometry/GeometryServer';  // TODO: make dynamic
//				var gsURL = 'http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer';  // TODO: make dynamic
				geometryService = new GeometryService(gsURL);

				gpsGraphicsLayer = new GraphicsLayer({id: 'gps-graphics-layer'});
				map.addLayer(gpsGraphicsLayer);
				gpsSymbol = new PictureMarkerSymbol('img/bluedot.png', 40, 40);
				gpsGraphic = new Graphic();
			},


			didSelectLayers: function (base, onLayers)
			{
				// Set base
				this.setBaseLayer(base);

				// Set lot line layer based on base
				this.setLotLineVisibility(onLayers.indexOf('ltl') > -1);

				// Set building outlines on/off
				this.setBuildingOutlineVisibility(onLayers.indexOf('bol') > -1);

				// Set legend layers
				// Make array of layer IDs that are only legend layers
				var lgdLyrsAll = _.pluck(_.where(ZMO.LAYERS.layers(), {type: 'legend'}), 'id'),
					lgdLyrs = _.intersection(onLayers, lgdLyrsAll);
				this.setLegendLayers(lgdLyrs);
			},

			findParcelForPIN: function (pin) {
				// Execute query task
				parcelQuery.where = "PIN = '" + pin + "'";
				parcelQueryTask.execute(parcelQuery, ZMO.MAP.handleParcelQueryResults);
			},

			handleGPSProject: function (projectParams) {
				if (projectParams.length > 0) {
					var newPoint = projectParams[0];
					gpsGraphic.setGeometry(newPoint);
					gpsGraphic.setSymbol(gpsSymbol);  // TODO: why do this every time?

					gpsGraphicsLayer.add(gpsGraphic);
					map.centerAndZoom(newPoint, 10);

					gpsIsOn = true;
				}
				else log('Error: could not project GPS coordinates)');
			},

			handleGPSUpdate: function (position) {
				log('GPS updated');
				var x = position.coords.longitude,
					y = position.coords.latitude,
					point = new Point(x, y),
					projectParams = new ProjectParameters();

				projectParams.geometries = [point];
				projectParams.outSR = map.spatialReference;

				geometryService.project(projectParams, ZMO.MAP.handleGPSProject);
			},

			handleParcelQueryResults: function (featureSet) {
				// Display map page
				var activePage = $.mobile.activePage.attr('id');
				if (activePage != 'home') {
//					$.mobile.navigate('#map');
					history.back();
				}

				try {
					map.graphics.clear();
					var features = featureSet.features;

					if (features.length == 0) {
						alert('No features to draw');
						return;
					}

					// Loop through features
					for (var i = 0; i < features.length; i++) {
						var feature = features[i];

						feature.setSymbol(parcelSymbol);

						// Get and set callout data
						var calloutData = ZMO.DATA.calloutDataForParcel(feature.attributes.PIN),
							attributes = feature.attributes;
						attributes.primaryAddress = calloutData.address;
						attributes.addresses = calloutData.addresses;
						attributes.zone = calloutData.zoneCode;
						attributes.landUse = calloutData.GPLU.join(', ');

						feature.setInfoTemplate(parcelInfoTemplate);

						map.graphics.add(feature);
					}

					// Zoom to extent of matching features
					var extent = esri.graphicsExtent(features);
					if (extent) {
						extent = extent.expand(3.0);
						map.setExtent(extent);
					}
				}
				catch(e) {
					log("Error drawing parcel query results", e.message);
				}
			},

			mapServiceBaseURL: function () {
				// TODO: do this
				return 'http://68wzimas1.ci.la.ca.us/ArcGIS/rest/services/'
			},

			// Takes a layer ID (e.g. 'zng') and sets the background layer; adds "include"  background layers
			// if necessary (e.g. D_GPLULINE)
			setBaseLayer: function (baseLyrID)
			{
				// If it's already set, stop
				if (cur.baseLyr) {
					if (cur.baseLyr.id == baseLyrID) return;
					map.removeLayer(cur.baseLyr);
				}

				// Create layer
				var svcName = ZMO.LAYERS.serviceNameForLayer(baseLyrID),
					url = this.urlForMapService(svcName),
					baseLyr = new ArcGISTiledMapServiceLayer(url, {id: baseLyrID});

				// Add new one
				map.addLayer(baseLyr, 0);

				// Set var
				cur.baseLyr = baseLyr;
			},

			setBuildingOutlineVisibility: function (shouldBecomeVisible)
			{
				// Turn on
				if (shouldBecomeVisible) {
					// Check if the layer exists
					if (!cur.bolLyr) {
						var svcName = ZMO.LAYERS.serviceNameForLayer('bol'),  // TODO: un-hard-code this
							url = this.urlForMapService(svcName),
							bolLyr = new ArcGISTiledMapServiceLayer(url, {id: 'bol'});
						map.addLayer(bolLyr);
						cur.bolLyr = bolLyr;
					}
					else if (!cur.bolLyr.visible) {
						// Turn on
						cur.bolLyr.show();
					}
				}

				// Turn off
				else if (cur.bolLyr && cur.bolLyr.visible) {
					cur.bolLyr.hide();
				}
				else {
					log(cur.bolLyr, cur.bolLyr.visible);
				}
			},

			setLotLineVisibility: function (shouldBecomeVisible)
			{
				var curLTLLyr = cur.ltlLyr;

				// Turn on
				if (shouldBecomeVisible) {
					// Check if there is already a layer and that it's the right layer
					var svcName = ZMO.LAYERS.serviceNameForLotLines(cur.baseLyr.id),
						url = this.urlForMapService(svcName);

					// If the layer already exists
					if (curLTLLyr) {
						// And it's already set right but not visible
						if (curLTLLyr.url === url) {
							if(!curLTLLyr.visible) {
								curLTLLyr.hide();
							}
						}
						else {
							// Remove old layer
							map.removeLayer(curLTLLyr);

							// Make and add new layer
							var ltlLyr = new ArcGISTiledMapServiceLayer(url, {id: 'ltl'});
							map.addLayer(ltlLyr);
							cur.ltlLyr = ltlLyr;
						}
					}

					// There is not currently an ltlLyr
					else {
						// Make and add new layer
						var ltlLyr = new ArcGISTiledMapServiceLayer(url, {id: 'ltl'});
						map.addLayer(ltlLyr);
						cur.ltlLyr = ltlLyr;
					}
				}

				// Turn off
				else if (cur.ltlLyr && cur.ltlLyr.visible) {
					cur.ltlLyr.hide();
				}
			},

			// Takes an array of layer IDs (e.g. 'eas') and sets the visible legend layer sub-layers
			setLegendLayers: function (lyrIDs)
			{
				// Get the base type
				var baseType = ZMO.LAYERS.typeForBaseLayer(cur.baseLyr.id),
					matchingLyrDefs = _.filter(layers, function(lyr) { return (lyrIDs.indexOf(lyr.id) > -1); }),
					lgdLyrIDs = _.map(matchingLyrDefs, function(def) { return def.legendLayerID[baseType]; }),
					lyr = cur.lgdLyr,
					url = this.urlForMapService('D_LEGENDLAYERS');

				// If there's no legend layer already
				if (!lyr) {
					// Set up legend layer
					var params = new esri.layers.ImageParameters();
					params.layerIds = lgdLyrIDs;
					params.layerOption = esri.layers.ImageParameters.LAYER_OPTION_SHOW;

					// Add and reference
					lyr = new ArcGISDynamicMapServiceLayer(url, {id: 'lgd', 'imageParameters': params});
					map.addLayer(lyr);
					cur.lgdLyr = lyr;
				}
				else {
					// Set visible layers
					lyr.setVisibleLayers(lgdLyrIDs);
				}
			},

			showParcelData: function () {
				log('showParcelData');

				// Get PIN
				// Get data
				// Format
				// Open new page
			},

			toggleGPS: function () {
				var $gpsButton = $('#gps-button');

				// TURN ON
				if (!gpsIsOn) {
					// Highlight button
					$gpsButton.removeClass('ui-btn-up-a');
					$gpsButton.addClass('ui-btn-up-b');

					// Get location
					if (navigator.geolocation) {
//						navigator.geolocation.getCurrentPosition(gpsUpdated);
						gpsWatchID = navigator.geolocation.watchPosition(ZMO.MAP.handleGPSUpdate, ZMO.MAP.handleGPSError);
					}

					else alert('Sorry, your browser does not support geolocation.');
				}

				// TURN OFF
				else {
					// Highlight button
					$gpsButton.removeClass('ui-btn-up-b');
					$gpsButton.addClass('ui-btn-up-a');

					// Stop watching
					navigator.geolocation.clearWatch(gpsWatchID);
					gpsGraphicsLayer.remove(gpsGraphic);
					gpsIsOn = false;
				}
			},

			// Takes a layer ID, returns the URL to the map service
			// ex: urlForLayer('gzn')
			urlForLayer: function (lyrID)
			{
				var layers = ZMO.config.layers,
					url;

				try {
					// Loop over layer groups
					for (groupName in layers) {
						// Loop over layers in layer group
						layers[groupName].forEach(function(lyr) {
							if (lyr.id == lyrID) {
								url = ZMO.MAP.urlForMapService(lyr.serviceName);
								throw(1);
							}
						});
					}
				}
				catch(e) {
					if (e !== 1) log("Error: could not get URL for layer with ID '" + lyrID + "'", e);
				}

				return url;
			},

			// Returns a URL for a map service
			// ex: urlForMapService('B_ZONING') => 'http://mapserver/ArcGIS/rest/services/B_ZONING'
			urlForMapService: function (svcName) {
				return ZMO.MAP.mapServiceBaseURL() + svcName + '/MapServer';
			}
		};

		ZMO.MAP.init();
	}
);
