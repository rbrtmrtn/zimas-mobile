ZMO.DATA = function()
{
	return {
		// Current state
		cur: {},

		// Takes a PIN and returns "preview" data for map callout
		calloutDataForParcel: function (pin) {
			// TODO: this doesn't work because of host-origin restrictions
			var url = 'http://zimas.ci.la.ca.us/ajaxPinInfo.aspx?request=address&pin=' + encodeURIComponent(pin),
				data = {url: url, requestType: 'get'},
				returnResponse;

			$.ajax({
				url: '../cgi-bin/zmo/proxy.py',
				type: 'post',
				data: data,
				async: false,
				success: function(response) {
					returnResponse = JSON.parse(response.response);
				}
			});

			return returnResponse;
		},

		// Takes a PIN and returns JSON data to render the parcel info page
		parcelDataForPin: function(pin) {
			return {
				pin: pin,
				data: {
					test: 'test'
				}
			}
		}
	};
}();