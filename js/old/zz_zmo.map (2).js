ZMO.MAP = function()
{
	var	map,
		baseURL,
		gpsIsOn = false,
		watchID,
		gpsPoint,
		gpsGraphic,
		gpsGraphicsLayer,
		geoService,
		lbQueryTask,
		lbQuery,
		lbInfoTemplate,
		lbSymbol,
		mtQueryTask,
		mtQuery,
		mtInfoTemplate,
		mtSymbol,
		ZMA_URL = 'http://68wzimas2.ci.la.ca.us/ArcGIS/rest/services',      // TODO: set this programmatically
		fl,
		flQuery;

	return {
		init: function() {
			// Set base URL
//			ZMO.MAP.getBaseURL();

			require([
				'esri/map',
				'esri/tasks/QueryTask',
				'esri/tasks/query',
				'esri/symbols/SimpleFillSymbol',
				'esri/symbols/SimpleLineSymbol',
				// 'esri/symbols/SimpleMarkerSymbol',
				'esri/symbols/PictureMarkerSymbol',
				'esri/InfoTemplate',
				'esri/geometry/Point',
				'esri/layers/GraphicsLayer',
				'esri/layers/FeatureLayer',
				'esri/graphic',
				'esri/tasks/GeometryService',
				'esri/dijit/AttributeInspector',
				'esri/SpatialReference',
				'dojo/_base/Color',
				'dojo/domReady!'
				],

				function(
					Map,
					QueryTask,
					Query,
					SimpleFillSymbol,
					SimpleLineSymbol,
					// SimpleMarkerSymbol,
					PictureMarkerSymbol,
					InfoTemplate,
					Point,
					GraphicsLayer,
					FeatureLayer,
					Graphic,
					GeometryService,
					AttributeInspector,
					SpatialReference,
					Color
				) {
					// Create map
					map = new Map('map', {
						center: new Point(6464228.876997188, 1844951.1482637103, new SpatialReference(2229)),
						logo: false,
						slider: false,
						zoom: 3
					});

					// Add default layers
					ZMO.MAP.addZIMASLayers();

					// Create query task
					lbQueryTask = new QueryTask(ZMA_URL + '/D_QUERYLAYERS/MapServer/5/query');

					// Create query
					lbQuery = new Query();
					lbQuery.returnGeometry = true;
					lbQuery.outFields = ['PIND'];

					// Create info template
					parcelInfoTemplate = new InfoTemplate('${PIND}', 'PIND: ${PIND}');

					// Create symbol
					parcelSymbol = new SimpleFillSymbol(
						SimpleFillSymbol.STYLE_SOLID,
						new SimpleLineSymbol(
							SimpleLineSymbol.STYLE_SOLID,
							new Color([255.0, 0.0, 0.0]),
							4.0
						),
						new Color([255.0, 0.0, 0.0, 0.0])
					);



					// MAPTILE QUERY TASK
					mtQueryTask = new QueryTask(ZMA_URL + '/D_QUERYLAYERS/MapServer/4/query');

					// Initialize query
					mtQuery = new Query();
					mtQuery.returnGeometry = true;
					mtQuery.outFields = [];

					// initialize info template
					mtInfoTemplate = new InfoTemplate();

					// Initialize symbol
					mtSymbol = new SimpleFillSymbol(
						SimpleFillSymbol.STYLE_SOLID,
						new SimpleLineSymbol(
							SimpleLineSymbol.STYLE_SOLID,
							new Color([255.0, 0.0, 0.0]),
							4.0
						),
						new Color([255.0, 0.0, 0.0, 0.0])
					);

					// TEST: feature layer
					var flURL = 'http://68wzimas1.ci.la.ca.us/ArcGIS/rest/services/D_QUERYLAYERS/MapServer/5';
					fl = new FeatureLayer(flURL, {
						mode: FeatureLayer.MODE_SELECTION,
						outFields: ['PIN', 'PIND']
					});

					fl.setSelectionSymbol(parcelSymbol);
					map.addLayer(fl);

					map.infoWindow.setContent('asdfadsfasdf');
					map.infoWindow.resize(375, 250);
					var ai = new AttributeInspector({
						layerInfos: [{'featureLayer': fl}]
					}, 'ai');

					flQuery = new Query();

					map.on('click', function(e) {
						log(e.mapPoint);
						flQuery.geometry = e.mapPoint;
						fl.selectFeatures(flQuery, FeatureLayer.SELECTION_NEW, function(fs) {
							map.infoWindow.setTitle(fs[0].getLayer().name);
							map.infoWindow.show(e.screenPoint, map.getInfoWindowAnchor(e.screenPoint));
						});
					});

				} // End of Dojo ready function
			); // End of Dojo require
		},

		addZIMASLayers: function()
		{
			// Names of ZIMAS map services
			svcNames = [
				'B_ZONING',
				'B_BLDGOUTLINES',
				'B_LOTLINES',
				// 'B_ORTHOLOTLINES',
				// 'B_GPLUPLY',
				// 'B_GPLULINE',
				// 'B_SCHOOLS',
				// 'B_ORTHOBW2011',
				// 'B_ORTHO4IN2006',
				// 'B_ORTHO4IN2008',
				// 'B_ORTHO2011',
				'D_LEGENDLAYERS',
				// 'D_ORTHOBASEMAPS',
				// 'D_STATIONFIRE',
				// 'D_GPLUPLY',
				// 'D_GPLULINE',
				// 'D_OVERVIEWMAPS',
				// 'D_BASEMAPS',
			];

			// Add layers
			svcNames.forEach(function(svcName)
			{
				var url = ZMA_URL + '/' + svcName + '/MapServer';
				var lyr;

				// Tiled
				if (svcName.substring(0, 1) == 'B') {
					lyr = new esri.layers.ArcGISTiledMapServiceLayer(url);
				}

				// D_LEGENDLAYERS
				else if (svcName == 'D_LEGENDLAYERS')
				{
					// Create image parameters (needed to specify layers)
					var params = new esri.layers.ImageParameters();
					params.layerIds = [0,10,22,53,63,73,125,96,108,83,93];
					params.layerOption = esri.layers.ImageParameters.LAYER_OPTION_SHOW;

					lyr = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
						'imageParameters': params
					});
				}

				// Dynamic
				else {
					log('Layer not added: ' + svcName);
				}
				
				// Add to map
				map.addLayer(lyr);
			});
		},

		drawFeatureSet: function(featureSet)
		{
			// Display map page
			var activePage = $.mobile.activePage.attr('id');
			if (activePage != 'home') {
				// $.mobile.navigate('#map-page');
				history.back();
			}

			try {
				// Clear graphics
				map.graphics.clear();

				var features = featureSet.features;

				// Check for no results
				if (features.length == 0) {
					alert('No features to draw');
					return;
				}

				// TEST
				map.graphics.on('graphic-draw', function(e) {
					log('graphic-draw', e);
				});

				map.graphics.on('error', function(e) {
					log('error', e);
				});

				map.graphics.on('update-start', function(e) {
					log('update-start', e);
				});

				// Loop through features
				for (var i = 0; i < features.length; i++) {
					var feature = features[i];
					feature.setSymbol(parcelSymbol);
					feature.setInfoTemplate(parcelInfoTemplate);

					map.graphics.add(feature);
				}

				// Zoom to extent of matching features
				var extent = esri.graphicsExtent(features);
				if (extent) {
					extent = extent.expand(3.0);
					map.setExtent(extent);
				}

			}
			catch(e) {
				log(e.message);
			}
		},

		getBaseURL: function() {
			var baseURL;

			$.ajax({
				url: 'http://zimas.ci.la.ca.us/dijits/zimasMap/config/ZimasMapConfig.xml',
				type: 'get',
				async: false,
				success: function(res) {
					log(res);
				}
			});
			return baseURL;
		},

		toggleGPS: function()
		{
			// TODO: put this in DOM
			var $gpsButton = $('#gps-button');

			// TURN ON
			if (!gpsIsOn) {
				// Highlight button
				$gpsButton.removeClass('ui-btn-up-a');
				$gpsButton.addClass('ui-btn-up-b');

				// Get location
				if (navigator.geolocation) {
					// navigator.geolocation.getCurrentPosition(gpsUpdated);
					watchID = navigator.geolocation.watchPosition(ZMO.MAP.gpsUpdated, ZMO.MAP.gpsReceivedError);
				}

				else {
					alert('Sorry, your browser does not support geolocation.');

					// map.graphics.remove(gpsGraphic);

					// gpsIsOn = false;
				}
			}

			// TURN OFF
			else {
				// Highlight button
				$gpsButton.removeClass('ui-btn-up-b');
				$gpsButton.addClass('ui-btn-up-a');

				// Stop watching
				navigator.geolocation.clearWatch(watchID);

				gpsGraphicsLayer.remove(gpsGraphic);

				gpsIsOn = false;
			}
		},

		gpsUpdated: function(position) {
			log('GPS updated');

			// Translate position to Geometry object
			gpsPoint.setLatitude(position.coords.latitude);
			gpsPoint.setLongitude(position.coords.longitude);

		    // Project gpsPoint to State Plane
			var projectParams = new esri.tasks.ProjectParameters();
		    projectParams.geometries = [gpsPoint];
		    projectParams.outSR = map.spatialReference;

		    geoService.project(projectParams, ZMO.MAP.handleGPSProject);
		},

		handleGPSProject: function(projectParams) {
			log('did project');
			if (projectParams.length > 0) {
				var newPoint = projectParams[0];
				gpsGraphic.setGeometry(newPoint);
				gpsGraphic.setSymbol(gpsSymbol);

				gpsGraphicsLayer.add(gpsGraphic);
				map.centerAndZoom(newPoint, 10);

		        gpsIsOn = true;
		    }
		},

		gpsReceivedError: function(error) {
			switch (error.code) {
				case error.PERMISSION_DENIED:
					console.log("Geolocation: permission denied");
					break;
				case error.POSITION_UNAVAILABLE:
					console.log("Geolocation: position unavailable");
					break;
				case error.TIMEOUT:
					console.log("Geolocation: timeout");
					break;
				default:
					console.log("Geolocation: unknown error");
					break;
			}
		},

		queryLandbase: function(pin) {
			// Execute query task
			lbQuery.where = "PIN = '" + pin + "'";
			lbQueryTask.execute(lbQuery, ZMO.MAP.drawFeatureSet);
		}

	}; // End of MAP
}();

ZMO.MAP.init();