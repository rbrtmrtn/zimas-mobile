ZMO.DOM = function()
{
	// Map
	var $mapPage,
		$gpsButton,

	// Layers
		$layersPage;

	return {
        init: function()
        {
            // Map
            $mapPage = $('#home');
            $gpsButton = $('#gps-button');

            $gpsButton.click(function()	{
                ZMO.MAP.toggleGPS();
            });

	        $('body').delegate('.parcel-data-link', 'click', function(e) {
		        ZMO.MAP.showParcelData();
	        });

	        /////////////
	        // Layers
	        /////////////

//			$layersPage = $('#layers');
//
//	        // Listen for page hide
//	        $layersPage.on('pagebeforehide', function(e)
//	        {
//		        // Get bol
//		        var bol = $('#checkbox-bol').is(':checked');
//
//		        // Get ltl
//		        var ltl = $('#checkbox-ltl').is(':checked');
//
//		        // Loop over others
//				var $lgd = $('input:checked').filter(':not(#checkbox-bol, #checkbox-ltl)'),
//					legendLyrs = [];
//
//		        $lgd.each(function(i) {
//			        var id = this.id;
//			        legendLyrs.push(id.substr(id.length - 3));
//		        });
//
//		        // Send
//		        ZMO.MAP.didSelectLayers(bol, ltl, legendLyrs);
//	        });
        }
	};
}();



// JQUERY
$(function() {
	ZMO.DOM.init();
});