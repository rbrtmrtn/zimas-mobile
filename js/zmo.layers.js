ZMO.LAYERS = function()
{
	var layers,             // Layer definitions
		onLayers = [],      // Layers currently on
		curBase,            // Currently selected base
		ZMO_LAYERS_JSON_PATH = 'js/zmo.layers.json',
		$checkboxes;

	return {
		init: function()
		{
			// Load defs
			this.loadLayerDefinitions();

			// Set base
			curBase = ZMO.config.defaultBase;

			// DOM STUFF

			// Reference all checkboxes
			$checkboxes = $('fieldset>input[type=checkbox]');

			// Set callbacks for checkboxes
			$checkboxes.on('click', function()
			{
				var layerID = this.dataset.layerId;
				var i = onLayers.indexOf(layerID);

				// Not currently on
				if (i == -1) onLayers.push(layerID);

				// Currently on
				else onLayers.splice(i, 1);
			});

			// Set checkboxes
			layers.forEach(function(layer) {
				var id = layer.id;
				if (layer.type != 'base') {
					var on = (ZMO.config.defaultLayers.indexOf(id) > -1);
//					layer.on = on;
					if (on) {
						onLayers.push(id);
						var $checkbox = $('#checkbox-' + id);
						if ($checkbox.length) {
							$checkbox.attr('checked', true);
						}
						else log("Error: could not get checkbox for layer '" + id + "'");
					}
				}
			});

			// Listen for page hide
			$('#layers').on('pagebeforehide', function(e)
			{
				// Update curBase
				curBase = $('#select-base').val();

//				ZMO.LAYERS.updateMap();
				ZMO.MAP.didSelectLayers(curBase, onLayers);
			});
		},

		definitionForLayer: function(id) {
			return _.where(layers, {id: id});
		},

		isOn: function(id) {
			return this.definitionForLayer(id)['on'];
		},

		layers: function() {
			return layers;
		},

		loadLayerDefinitions: function() {
			$.ajax({
				url: ZMO_LAYERS_JSON_PATH,
				type: 'get',
				async: false,
				success: function(data) {
					layers = data;
				},
				error: function(jqXHR, textStatus, errorThrown) {
					log('Could not load map configuration. Error: ' + textStatus);
				}
			});
		},

		mapLoaded: function() {
			ZMO.MAP.didSelectLayers(curBase, onLayers);
		},

		onLayers: function() {
//			return _.where(layers, {on: true});
		},

		serviceNameForLayer: function(id) {
			return _.where(layers, {id: id})[0]['serviceName'];
		},

		serviceNameForLotLines: function(baseLyrID) {
			var baseType = this.typeForBaseLayer(baseLyrID),
				ltlDef = _.where(layers, {id: 'ltl'})[0];
			return ltlDef.serviceName[baseType];
		},

		toggleLayer: function(id) {
			// Check if it's on onLayers
			// If it is, remove
			// If it isn't add it

			if (onLayers.indexOf(id) > -1) {

			}



//			var newVal;
//			for (var i = 0, length = layers.length; i < length; i++) {
//				var layer = layers[i];
//				if (layer.id == id) {
//					layer.on = !layer.on;
//					newVal = layer.on;
//				}
//			}
//			log(id, newVal);
//			return newVal;
		},

		typeForBaseLayer: function(id) {
			return _.pluck(_.where(layers, {id: id}), 'baseType')[0];
		},

		updateMap: function()
		{
//			var onIDs = _.pluck(this.onLayers(), 'id');

		}
	}
}();

$(function() {ZMO.LAYERS.init();});
