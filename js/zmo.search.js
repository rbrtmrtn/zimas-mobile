ZMO.SEARCH = function()
{
    var $searchType,
        $searchSub,
        $searchForm,
        $searchSubmit,
        $addressPopup,
        $addressPopupSelect,
        $addressPopupSubmit;

	return {
		init: function()
		{
			// Reference elements
			$searchType = $('#search-type');
			$searchSub = $('#search-sub');
			$searchForm = $('#search-form');
			$searchSubmit =  $('#search-submit');
			$addressPopup = $('#address-popup');
			$addressPopupSelect = $("select[name='address-popup-select']");
			$addressPopupSubmit = $('#address-popup-submit');

			// Connect elements
			$searchType.change(function() {
				ZMO.SEARCH.loadSearchSub();
			});

			$searchSubmit.click(function() {
				ZMO.SEARCH.submitSearch();
			});

			$addressPopupSubmit.click(function() {
				// Get new address
				var newAdd = $addressPopupSelect.val();

				// Close popup
				$addressPopup.popup('close');

				// Populate address field
				$('#address').val(newAdd);

				// Resubmit search
				ZMO.SEARCH.submitSearch();
			});
		},

		loadSearchSub: function()
		{
            // Get search type value
            var searchType = parseInt($('#search-type').val());

            // Handle search type
            switch(searchType)
            {
                // Address
                case 1:
                    $searchSub.html('\
						<label for="address">Address</label>\
						<input type="text" id="address" name="address" placeholder="ex. 200 N Spring St or 1st/Main">\
					');
                    $searchForm.trigger('create');
                    break;

                // PIN
                case 2:
                    $searchSub.html('\
						<label for="maptile">Maptile</label>\
						<input type="text" id="maptile" name="maptile" placeholder="ex. 165B173">\
						<label for="property">Property</label>\
						<input type="text" id="property" name="property" placeholder="ex. 645">\
					');
                    $searchForm.trigger('create');
                    break;

                // APN
                case 3:
                    $searchSub.html('\
						<label for="apn">APN</label>\
						<input type="text" id="apn" name="apn" placeholder="ex. 5161005906">\
					');
                    $searchForm.trigger('create');
                    break;

                // Legal Description
                case 4:
                    $searchSub.html('\
						<label for="tract">Tract</label>\
						<input type="text" id="tract" name="tract" placeholder="ex. TR 1842">\
						<label for="block">Block</label>\
						<input type="text" id="block" name="block" placeholder="ex. BLK B">\
						<label for="lot">Lot</label>\
						<input type="text" id="lot" name="lot" placeholder="ex. 20">\
					');
                    $searchForm.trigger('create');
                    break;

                // Maptile
                case 5:
                    $searchSub.html('\
						<label for="maptile">Tract</label>\
						<input type="text" id="maptile" name="maptile" placeholder="ex. 120B161">\
					');
                    $searchForm.trigger('create');
                    break;

                // Case Number
                case 6:
                    $searchSub.html('\
						<label for="case">Tract</label>\
						<input type="text" id="case" name="case" placeholder="ex. ZA-2013-1460-ZAA-SPP">\
					');
                    $searchForm.trigger('create');
                    break;

                // CPA
                case 7:
                    $searchSub.html('\
						<label for="cpa">Community Plan Area</label>\
						<select name="cpa" id="cpa">\
							<option>Arleta - Pacoima</option>\
							<option>Bel Air - Beverly Crest</option>\
						</select>\
					');
                    $searchForm.trigger('create');
                    break;

                // Council District
                case 8:
                    $searchSub.html('\
						<label for="cd">Council District</label>\
						<select name="cd" id="cd">\
							<option>CD 1 - Larry Christmas</option>\
							<option>CD 2 - Jay Leno</option>\
						</select>\
					');
                    $searchForm.trigger('create');
                    break;

                // Neighborhood Council
                case 9:
                    $searchSub.html('\
						<label for="nc">Council District</label>\
						<select name="nc" id="nc">\
							<option>Arleta</option>\
							<option>Arroyo Seco</option>\
						</select>\
					');
                    $searchForm.trigger('create');
                    break;
            }
		},

        parseAddress: function(add)
        {
            // Split into components
            var comps = add.split(' ');

            // Get house number
            var num = comps[0];

            // Validate house number
            if (num.search(/^[1-9](\d+)?$/) == -1) {   // Number of any length that does not start with 0
                return;
            }

            // Check for fractional (optional)
            var frac;
            if(comps[1].search('/') > -1) {
                frac = comps[1];
            }

            // Check for direction (optional)
            var dir;
            var dirCandidate = frac ? comps[2].toUpperCase() : comps[1].toUpperCase();

            if (dirCandidate in dirDict) {
                dir = dirDict[dirCandidate];
            }

            // Get street suffix (optional)
            var sfx;
            var lastComp = comps[comps.length - 1].toUpperCase()

            if (lastComp in sfxDict) {
                sfx = sfxDict[lastComp];
            }
            // else {
            // 	sfx = lastComp.toUpperCase();
            // }

            // STREET NAME
            var name;

            // Get index of first street name component
            var nameStart = 1;

            // Increase for fractional, direction
            nameStart = dir ? nameStart + 1 : nameStart;
            nameStart = frac ? nameStart + 1 : nameStart;

            // Get index of last street name component
            var nameEnd = sfx ? comps.length - 2 : comps.length - 1;

            // Make array of street name components
            var nameComps = [];

            for (var i = nameStart; i <= nameEnd; i++) {
                nameComps.push(comps[i].toUpperCase());
            }

            name = nameComps.join(' ');

            if (!name) {
                return;
            }

            // Return dictionary of components
            return {
                'num': num,
                'frac': frac,
                'dir': dir,
                'name': name,
                'sfx': sfx
            }
        },



        submitSearch: function()
        {
            // Get search type value
            var searchType = parseInt($('#search-type').val());

            // Handle search type
            switch(searchType)
            {
                // Address
                case 1:
                    var add = $('#address').val();

                    // Parse address
                    var params = ZMO.SEARCH.parseAddress(add);

                    // Check for null components
                    if (!params) {
                        alert('Please enter a valid address');
                        return;
                    }

                    // Post
                    $.post('../cgi-bin/zmo/pin_for_address.py', params, function(response)
                    {
                        log('response');
                        // Success
                        if (response.status == 200) {
                            log(response);
                            ZMO.MAP.findParcelForPIN(response.pin);
                        }

                        // Multiple results
                        else if (response.status == 300) {
                            var addresses = response.addresses;

                            // Clear dropdown first
                            $addressPopupSelect.empty();

                            // Populate address popup select
                            $.each(addresses, function(key, value) {
                                $addressPopupSelect.append('<option>' + value + '</option>');
                            });

                            // This makes the first item get selected
                            $addressPopupSelect.trigger('change');

                            // Show address popup
                            $addressPopup.popup('open');
                        }

                        // No results
                        else if (response.status == 400) {
                            alert('No results for that address. Please try again.');
                        }

                        // Server error
                        else if (response.status == 500) {
                            log('Could not resolve address: server error (' + response.error + ')');
                        }


                        // Unknown status
                        else {
                            log('Could not resolve address: unknown status')
                        }
                    });

                    break;



                // PIN
                case 2:

                    // Get input
                    var maptile = $('#maptile').val();
                    var property = $('#property').val();

                    // Form PIN
                    var numZeroes = 13 - maptile.length - property.length;
                    var pin = maptile.toUpperCase();

                    for (var i = 0; i < numZeroes; i++) {
                        pin += ' ';
                    }

                    pin += property;

                    // Execute query
                    ZMO.MAP.findParcelForPIN(pin);

                    break;

                // APN
                case 3:
                    // Get APN
                    var apn = $('#apn').val();

                    // Form params
                    var params = {'apn': apn};

                    // Post to CGI script
                    $.post('../cgi-bin/zmo/pin_for_apn.py', params, function(response)
                    {
                        // Single result
                        if (response.status == 200) {
                            // Execute query task
                            lbQuery.where = 'PIN = \'' + response.pin + '\'';
                            lbQueryTask.execute(lbQuery, drawFeatureSet);
                        }

                        // Multiple results
                        // TODO: this doesn't make any sense
                        else if (response.status == 300)
                        {
                            var pins = response.pins;

                            // Form where statement
                            var where = "PIN = '" + pins[0] + "'";

                            for (var i = 1; i < pins.length - 1; i++) {
                                var pin = pins[i];
                                where += "OR PIN = '" + pin + "'";
                            }

                            // Execute query
	                        log('Multiple results');
	                        alert('There was an error with your search. Please try again.');
//                            ZMO.MAP.findParcelForPIN(pin);
                        }

                        // No results
                        else if (response.status == 400) {
                            alert('No results for that APN. Please try again.');
                        }
                    });


                    break;

                // Legal Description
                case 4:
                    log('Search not handled');
                    break;

                // Maptile
                case 5:
                    // Get maptile
                    var maptile = $('#maptile').val();

                    // Execute query
                    mtQuery.where = "TILE_NAME = '" + maptile + "'";
                    mtQueryTask.execute(mtQuery, drawFeatureSet);

                    break;

                // Case Number
                case 6:

                    // Get case number
                    var caseNum = $('#case').val();

                    var params = {'case' : caseNum};

                    // Post to script
                    $.post('../cgi-bin/zmo/pin_for_case.py', params, function(response)
                    {
                        switch(response.status)
                        {
                            // Single result
                            case 200:
                                lbQuery.where = "PIN = '" + response.pin + "'";
                                lbQueryTask.execute(lbQuery, drawFeatureSet);
                                break;

                            // Multiple results
                            case 300:
                                print('Multiple results');
                                print(response);

                                break;

                            // No results
                            case 400:
                                break;
                        }
                    });

                    break;

                // CPA
                case 7:

                    break;

                // Council District
                case 8:

                    break;

                // Neighborhood Council
                case 9:

                    break;
            }
        }

	};
}();

$(ZMO.SEARCH.init);



/////////////////////////////
// OTHER FUNCTIONS
/////////////////////////////

function parseAddress(add)
{

}