ZMO.ROUTER = function()
{
	return {
		handlers: {
			'parcel-info': function(hash) {
				var pin = hash.query.pin,
					$page = $('#parcel-info'),
					$header = $('#parcel-info-header'),
					$content = $('#parcel-info-content');
				log(pin);



				var url = encodeURI('http://zimas.ci.la.ca.us/map.aspx?pin=' + pin + '&ajax=yes');
//				$.get(url, function(r) {
//					log(r);
//				});



//				$content.html(json.addressLegal.pin);
//				$page.page();
				$.mobile.changePage($page);

			},
		},

		// Takes a URL hash component with query string and parses into an object
		parseHash: function(hash) {
			var page = hash.replace(/\?.*$/, '').slice(1),
				qs = hash.replace(/^#.*(?=\?)/, '').slice(1),
				q = ZMO.ROUTER.parseQueryString(qs);
			return {
				page: page,
				query: q
			}
		},

		// Takes a URL query string and parses into an object
		parseQueryString: function(qs) {
			var match,
				plusPat = /\+/g,    // To replace plus signs with spaces
				queryPat = /([^&=]+)=?([^&]*)/g,
				decode = function (s) { return decodeURIComponent(s.replace(plusPat, ' ')); },
				args = {};
			while (match = queryPat.exec(qs)) {
				args[decode(match[1])] = decode(match[2]);
			}
			return args;
		}
	};
}();

// Bind to pagebeforechange event
// This is called twice during a page change - once before loading and once after
// First time data.toPage is set to a string, second time it's a jQuery object
$(document).on('pagebeforechange', function(e, data)
{
	// Make sure the page hasn't loaded yet
	if (typeof data.toPage === 'string')
	{
		var url = $.mobile.path.parseUrl(data.toPage),      // Parse URL
			hash = ZMO.ROUTER.parseHash(url.hash);          // Parse hash and query string

		// If there's a handler for that page
		if (hash.page in ZMO.ROUTER.handlers) {
			// Execute the handler
			ZMO.ROUTER.handlers[hash.page](hash);

			e.preventDefault();
		}
	}
});