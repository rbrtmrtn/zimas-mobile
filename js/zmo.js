ZMO = function()
{
	// Private
	ZMO_CONFIG_PATH = 'js/zmo.config.json';

	// Public
	return {
		init: function()
		{
			// Log function
			window.log=function(){log.history=log.history||[];log.history.push(arguments);if(this.console){console.log(Array.prototype.slice.call(arguments))}};

			// Load config
			$.ajax({
				url: ZMO_CONFIG_PATH,
				type: 'get',
				async: false,
				success: function(data) {
					ZMO.config = data;
				},
				error: function(jqXHR, textStatus, errorThrown) {
					log('Could not load map configuration. Error: ' + textStatus);
				}
			});
		}
	};
}();

ZMO.init();