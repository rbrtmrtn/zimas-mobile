#!python

import cgi
import urllib
import urllib2
import simplejson

# Get PIN
fs = cgi.FieldStorage()
# pin = fs.getvalue('pin')
pin = '132B193    16'

# Get data tabs for PIN
pin = urllib.quote(pin)
url = 'http://zimas.ci.la.ca.us/map.aspx?pin=%s&ajax=yes' % pin
r_zimas = urllib2.urlopen(url)
d = r_zimas.read()
j = simplejson.loads(d)

# Write out
# print "Content-Type: application/json\n"
print "Content-Type: text/html\n"

tabs = {
	'divTab1': 'Address/Legal',
	'divTab2': 'Jurisdictional',
	'divTab3': 'Planning and Zoning',
	'divTab4': 'Assessor',
	'divTab5': 'Case Numbers',
	'divTab6': 'Citywide Cases',
	'divTab7': 'Additional',
	'divTab8': 'Seismic Hazards',
	'divTab9': 'Economic Development Areas',
	'divTab10': 'Public Safety'
}

for tab in sorted(tabs.iterkeys()):
	tab_name = tabs[tab]

	# Create collapsible
	# print '<div data-role="collapsible" data-collapsed="false" data-inset="false" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u">'
	# print '<h3>' + tab_name + '</h3>'

	# Print table
	print type(j)
	break