#!python

import cgi
import cgitb
import pyodbc
import simplejson as json

# For debugging
cgitb.enable()

# Get POST data
fs = cgi.FieldStorage()

case = fs.getvalue('case')

# Connect to DB
db = pyodbc.connect('DSN=pcts;USER=CTSUSER;PWD=READ')
c = db.cursor()

# Form query
q = "SELECT PIN FROM CTS.TLA_PROP WHERE PROP_ID = (SELECT LOC_ID FROM CTS.TLOC WHERE APLC_ID = (SELECT APLC_ID FROM CTS.TCASE WHERE CASE_NBR = '%s'))" % case

# Execute
c.execute(q)
rows = c.fetchall()

# Write out
print "Content-Type: application/json\n"
# print "Content-Type: text/html\n"

r = {}

# Success
if len(rows) == 1:
	r['status'] = 200
	r['pin'] = rows[0][0]
# Multiple results
elif len(rows) > 1:
	r['status'] = 300
	pins = [];
	for row in rows:
		pins.append(row[0]);
	r['pins'] = pins;
# No results
else:
	r['status'] = 400

j = json.dumps(r)
print j