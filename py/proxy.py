#!python

import cgi
from urllib import urlencode
import urllib2
import simplejson

r = {}
data = None

try:
	# Get params
	fs = cgi.FieldStorage()
	url = fs.getvalue('url')
	request_type = fs.getvalue('requestType')

	# url = 'http://zimas.ci.la.ca.us/ajaxPinInfo.aspx?request=address&pin=132A213%20%20%20%2055'
	# request_type = 'get'

	if request_type == 'get':
		response = urllib2.urlopen(url).read()

		# Ghetto hack: massage crappy ZIMAS JSON
		clean = response.replace('\r\n', ' ')
		quoted = clean.replace('address:', '"address":').replace('addresses:', '"addresses":').replace('zoneCode:', '"zoneCode":').replace('GPLU:', '"GPLU":')
		r['response'] = quoted
		# j = simplejson.loads(quoted)
		# print simplejson.dumps(j)


	elif request_type == 'post':
		data = fs.getvalue('data')
		response = urllib2.urlopen(url, data).read()



		r['response'] = quoted
		r['status'] = 200

except Exception, e:
	r['status'] = 500
	r['error'] = 'Error executing request'

finally:
	print "Content-Type: application/json\n"
	print simplejson.dumps(r)