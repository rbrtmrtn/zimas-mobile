#!python

import cgi
import cgitb
from urllib import urlencode
import urllib2

# For debugging
cgitb.enable()

def post(url, data):
	enc = urlencode(data)
	r = urllib2.urlopen(url, enc)
	return r.read()

# Get params
fs = cgi.FieldStorage()
pin = fs.getvalue('pin')
# pin = '165B173-719'

url = 'http://68wzimas1.ci.la.ca.us/ArcGIS/rest/services/D_QUERYLAYERS/MapServer/5/query'
data = {
	'f': 'json',
	'where': "PIND = '%s'" % pin,
	'returnGeometry': True,
	# 'spatialRel': 'esriSpatialRelIntersects',
}

# Write out
print "Content-Type: application/json\n"
print post(url, data)