#!python

import cgi
import cgitb
import pyodbc
import simplejson as json

# For debugging
cgitb.enable()

# Get POST data
fs = cgi.FieldStorage()

num = fs.getvalue('num')
name = fs.getvalue('name')
frac = fs.getvalue('frac')
dir = fs.getvalue('dir')
sfx = fs.getvalue('sfx')

# num = '200'
# name = 'SPRING'
# dir = 'N'
# sfx = 'ST'
# frac = None

log = open('c:\zmolog.txt', 'a')

try:
	# Connect to DB
	db = pyodbc.connect('DSN=pcts;USER=ctsuser;PWD=read')
	c = db.cursor()

	# Form basic query
	# We are also selecting address elements in case there are multiple matches and we need the user to pick one
	q = "SELECT PIN, HSE_NBR, HSE_FRAC_NBR, HSE_DIR_CD, STR_NM, STR_SFX_CD FROM CTS.TLA_HSE_NBR WHERE HSE_NBR = %s AND STR_NM = '%s'" % (num, name)

	# Add optional criteria
	if frac:
		q += " AND HSE_FRAC_NBR = '%s'" % frac
	if dir:
		q += " AND HSE_DIR_CD = '%s'" % dir
	if sfx:
		q += " AND STR_SFX_CD = '%s'" % sfx

	# Order by direction
	q += 'ORDER BY HSE_DIR_CD'

	# Execute query
	c.execute(q)
	rows = c.fetchall()

	# Write out
	print "Content-Type: application/json\n"

	r = {}

	# Success
	if len(rows) == 1:	
		row = rows[0]
		r['status'] = 200
		r['pin'] = row[0]

		# Select only non-null address components; convert floats to strings (for house number)
		comps = [str(int(x)) if isinstance(x, float) else x for x in row[1:] if x]

		# Join components into string
		r['address'] = ' '.join(comps)

	# Multiple results
	elif len(rows) > 1:
		r['status'] = 300

		# Make array of address strings
		addresses = []

		# Loop over pin/address rows
		for row in rows:		
			# Select only non-null address components; convert floats to strings (for house number)
			comps = [str(int(x)) if isinstance(x, float) else x for x in row[1:] if x]

			# Join components into string
			address = ' '.join(comps)
			
			# Set PIN as value for address string
			addresses.append(address)

		r['addresses'] = addresses
		r['pins'] = [row[0] for row in rows]

	# No results
	else:
		r['status'] = 400

	j = json.dumps(r)
	print j

except Exception, e:
	# Write out
	print "Content-Type: application/json\n"
	r = {
		'status': 500,
		'error': str(e),
		# 'query': q,
	}
	print json.dumps(r)
