#!python

import cgi
import pyodbc
import simplejson as json
import zmo

# Finish function
def finish(status, msg, data):
	print "Content-Type: application/json\n"
	r = {
		'status': status,
		'msg': msg,
		'data': data,
	}
	print json.dumps(r)

# DB connection constructor
def Connection():
	try:
		cs = zmo.PCTS_CONNECTION_STRING()
		return pyodbc.connect(cs)
	except:
		finish(status=500, msg='Could not connect to PCTS')



'''
		QUERY FUNCTIONS

		These all take some arguments (args) and return a PIN number.
'''

def pin_for_address(args):
	# Connect to DB
	db = Connection()

	# Form base query
	q = """
		SELECT
			PIN,
			HSE_NBR,
			HSE_FRAC_NBR,
			HSE_DIR_CD,
			STR_NM,
			STR_SFX_CD
		FROM
			CTS.TLA_HSE_NBR
		WHERE
			HSE_NBR = ? AND
			STR_NM = ?
	"""

	# Get required args
	v = [
		args['num']
		args['name']
	]

	# Get optional args
	if 'frac' in args:
		q += " AND HSE_FRAC_NBR = ?"
		v.append(args['frac'])
	if 'dir' in args:
		q += " AND HSE_DIR_CD = ?"
		v.append(args['dir'])
	if 'sfx' in args:
		q += " AND STR_SFX_CD = ?"
		v.append(args['sfx'])

	# Execute query
	c.execute(q, v)
	rows = c.fetchall()

	# Success
	if len(rows) == 1:
		pin = rows[0][0]
		data = {'pin': pin}

		finish(status=200, data=data)

	# Multiple results
	elif len(rows) > 1:

		# Make array of address strings
		addresses = []

		# Loop over pin/address rows
		for row in rows:		
			# Select only non-null address components; convert floats to strings (for house number)
			comps = [str(int(x)) if isinstance(x, float) else x for x in row[1:] if x]

			# Join components into string
			address = ' '.join(comps)
			
			# Set PIN as value for address string
			addresses.append(address)

		pins = [row[0] for row in rows]
		data = {'addresses': addresses, 'pins': pins}

		finish(status=300, data=data)

	# No results
	else:
		finish(status=400, msg='No results')


# Runtime function
# Will not fire if this script is imported as a module
if __name__ == '__main__':

	# Get POST data
	fs = cgi.FieldStorage()
	search_type = fs.getvalue('searchType')
	args = fs.getvalue('args')

	# Connect to DB
	db = Connection()
	c = db.cursor()

	# Get function
	search_type_to_function = {
		'address' : pin_for_address,
		'apn': pin_for_apn,
		'case': pin_for_case,
	}
	search_function = search_type_to_function[search_type

	# Call function
	search_function(args)