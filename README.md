# ZIMAS Mobile #

A mobile version of ZIMAS -- the LA City Planning Department's enterprise web map.

### Notes ###

This code is not quite production-ready, but will serve as a good starting point for someone looking to create a mobile extension to ZIMAS. The front end is written in JavaScript and uses jQuery Mobile for UI and data calls. On the back end it relies on a few Python scripts to parse search queries and route them to the appropriate interfaces -- so a Python-enabled web server is necessary. I used a basic server stack for this (WAMP) and symlinked the `py` folder to Apache's `cgi-bin` directory. Not much other setup necessary.

### What Works ###

* The most common search functions (address, PIN, case number)
* The map identify task (clicking on a parcel to select)
* The layers selector

### What Doesn't Work ###

* Extended data for parcels (currently there's no API for this; ZIMAS renders server-side)
* Favorites (just didn't implement)

### Contact ###

Any questions, please [email](mailto:rmartin@rmart.in) me.